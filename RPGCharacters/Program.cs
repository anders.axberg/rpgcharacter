﻿using System;

namespace RPGCharacters
{
  internal class Program
  {
    /// <summary>
    /// create a character, equip items and print character sheet
    /// </summary>
    /// <param name="args"></param>
    static void Main(string[] args)
    {

      Warrior warrior = new Warrior("Wario Err");

      Weapon testAxe = new Weapon()
      {
        ItemName = "Common axe",
        ItemLevel = 2,
        ItemSlot = Slot.Slot_Weapon,
        WeaponType = WeaponType.Weapon_Axe,
        WeaponAttributes = new WeaponAttributes() { Damage = 7.0, AttackSpeed = 2.1 }
      };

      Armour testPlateBody = new Armour()
      {
        ItemName = "Common plate body armour",
        ItemLevel = 2,
        ItemSlot = Slot.Slot_Body,
        ArmourType = ArmourType.Armour_Plate,
        Attributes = new PrimaryAttributes() { Strength = 4 }
      };

      Armour testPlateHead = new Armour()
      {
        ItemName = "Common plate body helmet",
        ItemLevel = 2,
        ItemSlot = Slot.Slot_Head,
        ArmourType = ArmourType.Armour_Plate,
        Attributes = new PrimaryAttributes() { Strength = 2 }
      };
      Armour testPlateLegs = new Armour()
      {
        ItemName = "Common plate leg armour",
        ItemLevel = 2,
        ItemSlot = Slot.Slot_Legs,
        ArmourType = ArmourType.Armour_Plate,
        Attributes = new PrimaryAttributes() { Strength = 3 }
      };

      warrior.LevelUp();
      warrior.EquipItem(testAxe);
      warrior.EquipItem(testPlateBody);
      warrior.EquipItem(testPlateHead);
      warrior.EquipItem(testPlateLegs);
      warrior.CalculateTotalDamage();

      CharacterStats stats = new CharacterStats();
      Console.WriteLine(stats.PrintCharactertStats(warrior));

    }
  }
}
