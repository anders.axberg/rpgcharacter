﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
  /// <summary>
  /// Contains primary attribute properties, adds posibility for primary attributes addition and check if equal
  /// </summary>
  public class PrimaryAttributes
  {
    public double Strength { get; set; }
    public double Dexterity { get; set; }
    public double Intelligence { get; set; }

    /// <summary>
    /// Checks if two PrimaryAttributes are equal
    /// </summary>
    /// <param name="obj">PrimaryAttribute to check</param>
    /// <returns>True if attributes are equal</returns>
    public override bool Equals(object obj)
    {
      return obj is PrimaryAttributes attributes &&
             Strength == attributes.Strength &&
             Dexterity == attributes.Dexterity &&
             Intelligence == attributes.Intelligence;
    }
    /// <summary>
    /// Adds two PrimaryAttributes together
    /// </summary>
    /// <param name="lhs">First PrimaryAttribute to add</param>
    /// <param name="rhs">Second PrimaryAttribute to add</param>
    /// <returns>Sum of PrimaryAttributes</returns>
    public static PrimaryAttributes operator +(PrimaryAttributes lhs, PrimaryAttributes rhs)
    {
      return new PrimaryAttributes { Strength = lhs.Strength + rhs.Strength, Dexterity = lhs.Dexterity + rhs.Dexterity, 
        Intelligence = lhs.Intelligence + rhs.Intelligence  };
    }

  }
}
