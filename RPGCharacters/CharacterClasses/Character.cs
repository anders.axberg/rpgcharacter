﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
  /// <summary>
  /// Base class for character classes
  /// </summary>
  public abstract class Character
  {
    public string CharacterName { get; set; }
    public int CharacterLevel { get; set; }
    public PrimaryAttributes BasePrimaryAttribute { get; set; }
    public PrimaryAttributes LevelAttribute { get; set; }
    public Inventory Inventory { get; set; }

    public Character (string characterName)
    {
      CharacterName = characterName;
      CharacterLevel = 1;
      Inventory = new Inventory ();
    }
   
    /// <summary>
    /// Levels character up one level
    /// </summary>
    /// <returns>New character level</returns>
    public int LevelUp()
    {
      CharacterLevel++;
      BasePrimaryAttribute += LevelAttribute;
      return CharacterLevel;
    }
    /// <summary>
    /// Calculates total primary attributes for character, from base attributes + equipped armour attributes
    /// </summary>
    /// <returns>Total primary attributes</returns>
    public PrimaryAttributes TotalPrimaryAttributes()
    {
      PrimaryAttributes totalItemAttributes =BasePrimaryAttribute;
      foreach (KeyValuePair<Slot,Item> item in Inventory.EquippedItems)
      {
        if (item.Value is Armour armour)
        {
          totalItemAttributes = totalItemAttributes + armour.Attributes;
        }
      }
      return totalItemAttributes;
    }
    /// <summary>
    /// Calculates character damage, based on character level and eqiupped weapon
    /// </summary>
    /// <returns>Damage</returns>
    public double CalculateTotalDamage()
    {
      if (Inventory.EquippedItems.ContainsKey(Slot.Slot_Weapon))
      {
        Weapon weapon = Inventory.EquippedItems[Slot.Slot_Weapon] as Weapon;
        double DPS = weapon.WeaponAttributes.Damage * weapon.WeaponAttributes.AttackSpeed;
        return DPS * (1.0 + (ClassTotalPrimaryAttribute(TotalPrimaryAttributes())/100));
      }
      return 1.0 * (1.0 + (ClassTotalPrimaryAttribute(TotalPrimaryAttributes())/100));
    }

    /// <summary>
    /// Equips item in appropriate slot, if correct level and type for character
    /// </summary>
    /// <param name="item">Item to be equipped</param>
    /// <returns>Item added message</returns>
    /// <exception cref="InvalidWeaponException">Weapon level too high, or wrong weapon type</exception>
    /// <exception cref="InvalidArmourException">Armour level too high, or wrong armour type</exception>
    public string EquipItem(Item item)
    {

      if (item is Weapon weapon)
      {
        if (item.ItemLevel > CharacterLevel)
        {
          throw new InvalidWeaponException("Character level is too low to equip this item");
        }

        if (CheckWeaponType(weapon))
        {
          Inventory.EquippedItems.Add(item.ItemSlot, item);
          return "New weapon equipped!";
        }
        else
        {
          throw new InvalidWeaponException("This character can not use this weapon type");
        }
      }

      if (item is Armour armour)
      {
        if (item.ItemLevel > CharacterLevel)
        {
          throw new InvalidArmourException("Character level is too low to equip this item");
        }

        if (CheckArmourType(armour))
        {
          Inventory.EquippedItems.Add(item.ItemSlot, item);
          return "New armour equipped!";
        }
        else
        {
          throw new InvalidArmourException("This character can not use this armour type");
        }
      }
      return "Nothing was equipped";

    }
    /// <summary>
    /// Checks if character can equip this weapon type
    /// </summary>
    /// <param name="weapon">Weapon to be equipped</param>
    /// <returns>True if weapon is equippable</returns>
    protected abstract bool CheckWeaponType(Weapon weapon);
    /// <summary>
    /// Checks if character can equip this armour type
    /// </summary>
    /// <param name="armour">Armour to be equipped</param>
    /// <returns>True if armour is equippable</returns>
    protected abstract bool CheckArmourType(Armour armour);
    /// <summary>
    /// Finds the primary attribute for the character class
    /// </summary>
    /// <param name="totalPrimaryattribute">Current total primary attributes</param>
    /// <returns>Primary attribute for the class</returns>
    protected abstract double ClassTotalPrimaryAttribute(PrimaryAttributes totalPrimaryattribute);
  }
}
