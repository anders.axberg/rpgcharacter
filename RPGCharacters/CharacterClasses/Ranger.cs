﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
  /// <summary>
  /// Ranger class, inherits from Character class, adds specific overrides for ranger class
  /// </summary>
  public class Ranger : Character
  {    
    public Ranger(string characterName) : base(characterName)
    {
      BasePrimaryAttribute = new PrimaryAttributes() { Strength = 1, Dexterity = 7, Intelligence = 1 };
      LevelAttribute = new PrimaryAttributes() { Strength = 1, Dexterity = 5, Intelligence = 1 };
    }
    protected override bool CheckWeaponType(Weapon weapon)
    {
      if (weapon.WeaponType == WeaponType.Weapon_Bow)
      {
        return true;
      }
      return false;
    }
    protected override bool CheckArmourType(Armour armour)
    {
      if (armour.ArmourType == ArmourType.Armour_Mail || armour.ArmourType == ArmourType.Armour_Leather)
      {
        return true;
      }
      return false;
    }
    protected override double ClassTotalPrimaryAttribute(PrimaryAttributes totalPrimaryAttributes)
    {
      return totalPrimaryAttributes.Dexterity;
    }
  }
}
