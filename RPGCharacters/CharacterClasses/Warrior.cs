﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
  public class Warrior : Character
  {
    /// <summary>
    /// Warrior class, inherits from Character class, adds specific overrides for warrior class
    /// </summary>
    public Warrior(string characterName) : base(characterName)
    {
      BasePrimaryAttribute = new PrimaryAttributes() { Strength = 5, Dexterity = 2, Intelligence = 1 };
      LevelAttribute = new PrimaryAttributes() { Strength = 3, Dexterity = 2, Intelligence = 1 };
    }
    protected override bool CheckWeaponType(Weapon weapon)
    {
      if (weapon.WeaponType == WeaponType.Weapon_Axe || weapon.WeaponType == WeaponType.Weapon_Hammer || weapon.WeaponType == WeaponType.Weapon_Sword)
      {
        return true;
      }
      return false;
    }
    protected override bool CheckArmourType(Armour armour)
    {
      if (armour.ArmourType == ArmourType.Armour_Mail || armour.ArmourType == ArmourType.Armour_Plate)
      {
        return true;
      }
      return false;
    }
    protected override double ClassTotalPrimaryAttribute(PrimaryAttributes totalPrimaryAttributes)
    {
      return totalPrimaryAttributes.Strength;
    }
  }
}
