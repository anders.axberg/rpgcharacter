﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
  public class CharacterStats
  {
    /// <summary>
    /// Create character sheet
    /// </summary>
    /// <param name="character">Current character</param>
    /// <returns>Strin containing character stats</returns>
    public string PrintCharactertStats(Character character)
  {
      PrimaryAttributes totalstats = character.TotalPrimaryAttributes();
      StringBuilder Charactersheet = new StringBuilder();
      Charactersheet.Append($"Name: {character.CharacterName} \n");
      Charactersheet.Append($"Level: {character.CharacterLevel} \n");
      Charactersheet.Append($"Strength: {totalstats.Strength}  \n");
      Charactersheet.Append($"Dexterity: {totalstats.Dexterity}  \n");
      Charactersheet.Append($"Intelligence: {totalstats.Intelligence}  \n");
      Charactersheet.Append($"Damage: {character.CalculateTotalDamage()}");
      return Charactersheet.ToString();
  }


  }
}
