﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
  /// <summary>
  /// Mage class, inherits from Character class, adds specific overrides for mage class
  /// </summary>
  public class Mage : Character
  {
    public Mage(string characterName) : base(characterName)
    {
      BasePrimaryAttribute = new PrimaryAttributes() { Strength = 1, Dexterity = 1, Intelligence = 8 };
      LevelAttribute = new PrimaryAttributes() { Strength = 1, Dexterity = 1, Intelligence = 5 };
    }

    protected override bool CheckWeaponType(Weapon weapon)
    {
      if (weapon.WeaponType == WeaponType.Weapon_Staff || weapon.WeaponType == WeaponType.Weapon_Wand)
      {
        return true;
      }
      return false; 
    }

    protected override bool CheckArmourType(Armour armour)
    {
      if (armour.ArmourType == ArmourType.Armour_Cloth)
      {
        return true;
      }
      return false;
    }

    protected override double ClassTotalPrimaryAttribute(PrimaryAttributes totalPrimaryAttributes)
    {
      return totalPrimaryAttributes.Intelligence;
    }
  }
}

