﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
  /// <summary>
  /// Exception thrown if weapon level too high or wrong type for character
  /// </summary>
  public class InvalidWeaponException : Exception
  {
    public InvalidWeaponException(string message) : base(message)
    {

    }
  }
}
