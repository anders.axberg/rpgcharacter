﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
  /// <summary>
  /// Attribute properties for weapons
  /// </summary>
  public class WeaponAttributes
  {
    public double Damage { get; set; }
    public double AttackSpeed { get; set; }
  }
}
