﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
  /// <summary>
  /// Exception thrown if armour level too high or wrong type for character
  /// </summary>
  public class InvalidArmourException : Exception
  {
    public InvalidArmourException(string message) : base(message)
    {

    }
  }
}
