﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
  /// <summary>
  /// Defines the different types of armour
  /// </summary>
  public enum ArmourType
  {
    Armour_Cloth,
    Armour_Leather,
    Armour_Mail,
    Armour_Plate,

  }
}
