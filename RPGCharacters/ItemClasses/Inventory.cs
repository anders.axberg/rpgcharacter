﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
  /// <summary>
  /// Inventory containing equipped items in separate slots depending on type
  /// </summary>
  public class Inventory
  {
    public Dictionary<Slot,Item> EquippedItems { get; set; }

    public Inventory()
    {
      EquippedItems = new Dictionary<Slot,Item>(); 
    }
  }
}
