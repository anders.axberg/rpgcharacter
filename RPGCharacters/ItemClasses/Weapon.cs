﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
  /// <summary>
  /// Defines weapon item, with attributes and weapon type
  /// </summary>
  public class Weapon : Item
  {
    public WeaponType WeaponType { get; set; }
    public WeaponAttributes WeaponAttributes { get; set; }
  }
}
