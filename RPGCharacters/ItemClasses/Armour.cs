﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
  /// <summary>
  /// Defines Armour item, with attributes and armour type
  /// </summary>
  public class Armour : Item
  {
    public ArmourType ArmourType { get; set; }
    public PrimaryAttributes Attributes { get; set; }


  }
}
