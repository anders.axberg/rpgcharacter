﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
  /// <summary>
  /// defines the different slot types
  /// </summary>
  public enum Slot
  {
    Slot_Head,
    Slot_Body,
    Slot_Legs,
    Slot_Weapon,

  }
}
