﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
  /// <summary>
  /// defines the different weapon types
  /// </summary>
  public enum WeaponType
  {
    Weapon_Axe,
    Weapon_Bow,
    Weapon_Dagger,
    Weapon_Hammer,
    Weapon_Staff,
    Weapon_Sword,
    Weapon_Wand,

  }
}
