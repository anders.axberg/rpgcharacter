﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
  /// <summary>
  /// Main class for Items, armour and weapon inherits from this class
  /// </summary>
  public abstract class Item
  {
    public string ItemName { get; set; }
    public int ItemLevel { get; set; }
    public Slot ItemSlot { get; set; }
  }
}
