﻿using System;
using Xunit;
using RPGCharacters;

namespace RPGCharactersTests
{
  public class ItemTests
  {
    /// <summary>
    /// Test if InvalidWeaponException is thrown when weapon level is too high for character
    /// </summary>
    [Fact]
    public void EquipItem_EquipWeaponWithTooHighLevel_ShouldThrowInvalidWeaponException()
    {
      //Arrange
      Warrior warrior= new Warrior("Wario Err");
      Weapon testAxe = new Weapon()
      {
        ItemName = "Common axe",
        ItemLevel = 2,
        ItemSlot = Slot.Slot_Weapon,
        WeaponType = WeaponType.Weapon_Axe,
        WeaponAttributes = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }
      };

      //Act      

      //Assert
      Assert.Throws<InvalidWeaponException>(() => warrior.EquipItem(testAxe));
    }

    /// <summary>
    /// Test if InvalidArmourException is thrown when armour level is too high for character
    /// </summary>
    [Fact]
    public void EquipItem_EquipArmourWithTooHighLevel_ShouldThrowInvalidArmourException()
    {
      //Arrange
      Warrior warrior = new Warrior("Wario Err");
      Armour testPlateBody = new Armour()
      {
        ItemName = "Common plate body armour",
        ItemLevel = 2,
        ItemSlot = Slot.Slot_Body,
        ArmourType = ArmourType.Armour_Plate,
        Attributes = new PrimaryAttributes() { Strength = 1 }
      };

      //Act


      //Assert
      Assert.Throws<InvalidArmourException>(() => warrior.EquipItem(testPlateBody));
    }

    /// <summary>
    /// Test if InvalidWeaponException is thrown when weapon type is not equippable character
    /// </summary>
    [Fact]
    public void EquipItem_EquipWrongWeaponType_ShouldThrowInvalidWeaponException()
    {
      //Arrange
      Warrior warrior = new Warrior("Wario Err");
      Weapon testBow = new Weapon()
      {
        ItemName = "Common bow",
        ItemLevel = 1,
        ItemSlot = Slot.Slot_Weapon,
        WeaponType = WeaponType.Weapon_Bow,
        WeaponAttributes = new WeaponAttributes() { Damage = 12, AttackSpeed = 0.8 }
      };

      //Act


      //Assert
      Assert.Throws<InvalidWeaponException>(() => warrior.EquipItem(testBow));
    }

    /// <summary>
    /// Test if InvalidArmourException is thrown when armour type is not equippable character
    /// </summary>
    [Fact]
    public void EquipItem_EquipWrongArmourType_ShouldThrowInvalidArmourException()
    {
      //Arrange
      Warrior warrior = new Warrior("Wario Err");
      Armour testClothHead = new Armour()
      {
        ItemName = "Common cloth head armor",
        ItemLevel = 1,
        ItemSlot = Slot.Slot_Head,
        ArmourType = ArmourType.Armour_Cloth,
        Attributes = new PrimaryAttributes() { Intelligence = 5 }
      };

      //Act


      //Assert
      Assert.Throws<InvalidArmourException>(() => warrior.EquipItem(testClothHead));
    }

    /// <summary>
    /// Test if New weapon equipped message is returned when weapon is successfully equipped
    /// </summary>
    [Fact]
    public void EquipItem_EquipValidWeapon_ReturnsNewWeaponEquippedMessage()
    {
      //Arrange
      Warrior warrior = new Warrior("Wario Err");
      Weapon testAxe = new Weapon()
      {
        ItemName = "Common axe",
        ItemLevel = 1,
        ItemSlot = Slot.Slot_Weapon,
        WeaponType = WeaponType.Weapon_Axe,
        WeaponAttributes = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }
      };
      string expected = "New weapon equipped!";

      //Act
      string actual = warrior.EquipItem(testAxe);

      //Assert
      Assert.Equal(expected, actual);
    }


    /// <summary>
    /// Test if New armour equipped message is returned when armour is successfully equipped
    /// </summary>
    [Fact]
    public void EquipItem_EquipValidArmour_ReturnsNewArmourEquippedMessage()
    {
      //Arrange
      Warrior warrior = new Warrior("Wario Err");
      Armour testPlateBody = new Armour()
      {
        ItemName = "Common plate body armour",
        ItemLevel = 1,
        ItemSlot = Slot.Slot_Body,
        ArmourType = ArmourType.Armour_Plate,
        Attributes = new PrimaryAttributes() { Strength = 1 }
      };
      string expected = "New armour equipped!";

      //Act
      string actual = warrior.EquipItem(testPlateBody);

      //Assert
      Assert.Equal(expected, actual);
    }

    /// <summary>
    /// Test if correct damage value for character without weapon is calculated
    /// </summary>
    [Fact]
    public void CalculateDamage_CalculateDamageWithoutWeapon_ReturnsExpectedDamage()
    {
      //Arrange
      Warrior warrior = new Warrior("Wario Err");
      double expected = 1.0 * (1.0+(5.0 / 100.0));
      //Act
      double actual = warrior.CalculateTotalDamage();
      //Assert
      Assert.Equal(expected,actual);
    }

    /// <summary>
    /// Test if correct damage value for character with weapon is calculated
    /// </summary>
    [Fact]
    public void CalculateDamage_CalculateDamageWithWeapon_ReturnsExpectedDamage()
    {
      //Arrange
      Warrior warrior = new Warrior("Wario Err");
      Weapon testAxe = new Weapon()
      {
        ItemName = "Common axe",
        ItemLevel = 1,
        ItemSlot = Slot.Slot_Weapon,
        WeaponType = WeaponType.Weapon_Axe,
        WeaponAttributes = new WeaponAttributes() { Damage = 7.0, AttackSpeed = 1.1 }
      };
      double expected = (7.0 * 1.1)*(1.0 + (5.0 / 100.0));

      //Act
      warrior.EquipItem(testAxe);
      double actual = warrior.CalculateTotalDamage();
      //Assert
      Assert.Equal(expected, actual);
    }

    /// <summary>
    /// Test if correct damage value for character with weapon and armour is calculated
    /// </summary>
    [Fact]
    public void CalculateDamage_CalculateDamageWithWeaponAndArmour_ReturnsExpectedDamage()
    {
      //Arrange
      Warrior warrior = new Warrior("Wario Err");

      Weapon testAxe = new Weapon()
      {
        ItemName = "Common axe",
        ItemLevel = 1,
        ItemSlot = Slot.Slot_Weapon,
        WeaponType = WeaponType.Weapon_Axe,
        WeaponAttributes = new WeaponAttributes() { Damage = 7.0, AttackSpeed = 1.1 }
      };

      Armour testPlateBody = new Armour()
      {
        ItemName = "Common plate body armour",
        ItemLevel = 1,
        ItemSlot = Slot.Slot_Body,
        ArmourType = ArmourType.Armour_Plate,
        Attributes = new PrimaryAttributes() { Strength = 1 }
      };

      double expected = (7.0 * 1.1) * (1.0 + ((5.0+1.0) / 100.0));

      //Act
      warrior.EquipItem(testAxe);
      warrior.EquipItem(testPlateBody);
      double actual = warrior.CalculateTotalDamage();

      //Assert
      Assert.Equal(expected, actual);
    }


    /// <summary>
    /// Test if correct damage value for level 2 character with weapon and full armour set is calculated
    /// </summary>
    [Fact]
    public void CalculateDamage_CalculateDamageWithWeaponAndFullArmourAndLevelUp_ReturnsExpectedDamage()
    {
      //Arrange
      Warrior warrior = new Warrior("Wario Err");

      Weapon testAxe = new Weapon()
      {
        ItemName = "Common axe",
        ItemLevel = 2,
        ItemSlot = Slot.Slot_Weapon,
        WeaponType = WeaponType.Weapon_Axe,
        WeaponAttributes = new WeaponAttributes() { Damage = 7.0, AttackSpeed = 2.1 }
      };

      Armour testPlateBody = new Armour()
      {
        ItemName = "Common plate body armour",
        ItemLevel = 2,
        ItemSlot = Slot.Slot_Body,
        ArmourType = ArmourType.Armour_Plate,
        Attributes = new PrimaryAttributes() { Strength = 4 }
      };

      Armour testPlateHead = new Armour()
      {
        ItemName = "Common plate body helmet",
        ItemLevel = 2,
        ItemSlot = Slot.Slot_Head,
        ArmourType = ArmourType.Armour_Plate,
        Attributes = new PrimaryAttributes() { Strength = 2 }
      };
      Armour testPlateLegs = new Armour()
      {
        ItemName = "Common plate leg armour",
        ItemLevel = 2,
        ItemSlot = Slot.Slot_Legs,
        ArmourType = ArmourType.Armour_Plate,
        Attributes = new PrimaryAttributes() { Strength = 3}
      };

      double expected = (7.0 * 2.1) * (1.0 + ((8.0 + 4.0 + 2.0 + 3.0) / 100.0));
    
      //Act
      warrior.LevelUp();
      warrior.EquipItem(testAxe);
      warrior.EquipItem(testPlateBody);
      warrior.EquipItem(testPlateHead);
      warrior.EquipItem(testPlateLegs);
      double actual = warrior.CalculateTotalDamage();
      
      //Assert
      Assert.Equal(expected, actual);
    }
  }
}
