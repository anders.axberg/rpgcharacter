using System;
using Xunit;
using RPGCharacters;

namespace RPGCharactersTests
{
  public class CharacterTests
  {
    /// <summary>
    /// Test if character is level 1 after being created
    /// </summary>
    [Fact]
    public void Character_CreateCharacter_ShouldBeLevel1()
    {
      //Arrange
      Warrior genericCharacter = new Warrior("Wario Err");
      int expected = 1;

      //Act
      int actual = genericCharacter.CharacterLevel;

      //Assert
      Assert.Equal(expected, actual); 
    }

    /// <summary>
    /// Test if level increases by 1 after leveling up
    /// </summary>
    [Fact]
    public void LevelUp_CharacterLevelsUp_ShoudIncreaseLevelBy1()
    {
      //Arrange
      Warrior genericCharacter = new Warrior("Wario Err");
      int expected = 2;

      //Act
      int actual = genericCharacter.LevelUp();

      //Assert
      Assert.Equal(expected, actual);
    }


    #region Test child character classe base attributes    
    /// <summary>
    /// Test if mage is created with correct attributes
    /// </summary>
    [Fact]
    public void Mage_CreateMage_ShouldHaveDefaultAttributes()
    {
      //Arrange
      Mage genericMage = new Mage("Mage Ellan");
      PrimaryAttributes primaryAttributes = new PrimaryAttributes() { Strength = 1, Dexterity = 1, Intelligence = 8 };
      bool expected = true;

      //Act
      bool actual = primaryAttributes.Equals(genericMage.BasePrimaryAttribute);

      //Assert
      Assert.Equal(actual, expected);
    }

    /// <summary>
    /// Test if ranger is created with correct attributes
    /// </summary>
    [Fact]
    public void Ranger_CreateRanger_ShouldHaveDefaultAttributes()
    {
      //Arrange
      Ranger genericRanger = new Ranger("Rain Gur");
      PrimaryAttributes primaryAttributes = new PrimaryAttributes() { Strength = 1, Dexterity = 7, Intelligence = 1 };
      bool expected = true;

      //Act
      bool actual = primaryAttributes.Equals(genericRanger.BasePrimaryAttribute);

      //Assert
      Assert.Equal(actual, expected);
    }

    /// <summary>
    /// Test if rogue is created with correct attributes
    /// </summary>
    [Fact]
    public void Rogue_CreateRogue_ShouldHaveDefaultAttributes()
    {
      //Arrange
      Rogue genericRogue = new Rogue("Roe Guh");
      PrimaryAttributes primaryAttributes = new PrimaryAttributes() { Strength = 2, Dexterity = 6, Intelligence = 1 };
      bool expected = true;

      //Act
      bool actual = primaryAttributes.Equals(genericRogue.BasePrimaryAttribute);

      //Assert
      Assert.Equal(actual, expected);
    }

    /// <summary>
    /// Test if warrior is created with correct attributes
    /// </summary>
    [Fact]
    public void Warrior_CreateWarrior_ShouldHaveDefaultAttributes()
    {
      //Arrange
      Warrior genericWarrior = new Warrior("Roe Guh");
      PrimaryAttributes primaryAttributes = new PrimaryAttributes() { Strength = 5, Dexterity = 2, Intelligence = 1 };
      bool expected = true;

      //Act
      bool actual = primaryAttributes.Equals(genericWarrior.BasePrimaryAttribute);

      //Assert
      Assert.Equal(actual, expected);
    }

    #endregion

    #region Test child class levelUp

    /// <summary>
    /// Test if mage has correct attributes after leveling up
    /// </summary>
    [Fact]
    public void LevelUp_LevelUpMageToLevel2_ShouldHaveLevel2baseAttributes()
    {
      //Arrange
      Mage genericMage = new Mage("Mage Ellan");
      PrimaryAttributes baseAttributesLevel2 = new PrimaryAttributes() { Strength = 2, Dexterity = 2, Intelligence = 13 };
      bool expected = true;

      //Act
      genericMage.LevelUp();
      bool actual = baseAttributesLevel2.Equals(genericMage.BasePrimaryAttribute);

      //Assert
      Assert.Equal(actual, expected);
    }

    /// <summary>
    /// Test if ranger has correct attributes after leveling up
    /// </summary>
    [Fact]
    public void LevelUp_LevelUpRangerToLevel2_ShouldHaveLevel2baseAttributes()
    {
      //Arrange
      Ranger genericRanger = new Ranger("Rain Gur");
      PrimaryAttributes baseAttributesLevel2 = new PrimaryAttributes() { Strength = 2, Dexterity = 12, Intelligence = 2 };
      bool expected = true;

      //Act
      genericRanger.LevelUp();
      bool actual = baseAttributesLevel2.Equals(genericRanger.BasePrimaryAttribute);

      //Assert
      Assert.Equal(actual, expected);
    }

    /// <summary>
    /// Test if rogue has correct attributes after leveling up
    /// </summary>
    [Fact]
    public void LevelUp_LevelUpRogueToLevel2_ShouldHaveLevel2baseAttributes()
    {
      //Arrange
      Rogue genericRogue = new Rogue("Roe Guh");
      PrimaryAttributes baseAttributesLevel2 = new PrimaryAttributes() { Strength = 3, Dexterity = 10, Intelligence = 2 };
      bool expected = true;

      //Act
      genericRogue.LevelUp();
      bool actual = baseAttributesLevel2.Equals(genericRogue.BasePrimaryAttribute);

      //Assert
      Assert.Equal(actual, expected);
    }

    /// <summary>
    /// Test if warrior has correct attributes after leveling up
    /// </summary>
    [Fact]
    public void LevelUp_LevelUpWarriorToLevel2_ShouldHaveLevel2baseAttributes()
    {
      //Arrange
      Warrior genericWarrior = new Warrior("Wario Err");
      PrimaryAttributes baseAttributesLevel2 = new PrimaryAttributes() { Strength = 8, Dexterity = 4, Intelligence = 2 };
      bool expected = true;

      //Act
      genericWarrior.LevelUp();
      bool actual = baseAttributesLevel2.Equals(genericWarrior.BasePrimaryAttribute);

      //Assert
      Assert.Equal(actual, expected);
    }
    #endregion
  }
}
