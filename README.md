# RPGCharacters

## Description
Assignment 1. Create character classes for an RPG game

## Installation
Visual studio 2022. .Net version 5.0

## Usage
Run units test, run with debug to print character sheet


## Class diagram

![Class diagram](RPGCharacters/ClassDiagram1.png)

